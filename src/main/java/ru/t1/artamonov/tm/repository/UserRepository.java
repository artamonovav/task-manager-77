package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull String login);

}
