package ru.t1.artamonov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.t1.artamonov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.artamonov.tm.api.endpoint.ITaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
public class SoapEndpointConfig {

    @Bean
    public Endpoint projectEndpointRegistry(final IProjectEndpoint projectEndpoint, final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final ITaskEndpoint taskEndpoint, final Bus bus) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}
