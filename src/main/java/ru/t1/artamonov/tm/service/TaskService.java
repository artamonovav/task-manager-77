package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.repository.TaskRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public void add(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void addByUserId(@Nullable final String userId, @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public Task findByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void delete(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable final String userId, @Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (findById(model.getId()) == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void updateByUserId(@Nullable final String userId, @Nullable final Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (findByUserIdAndId(userId, model.getId()) == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

}
