package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    void delete(Task task);

    @WebMethod
    void deleteById(String id);

    @Nullable
    @WebMethod
    Collection<Task> findAll();

    @Nullable
    @WebMethod
    Task findById(String id);

    @NotNull
    @WebMethod
    Task create(Task task);

    @NotNull
    @WebMethod
    Task update(Task task);

}
